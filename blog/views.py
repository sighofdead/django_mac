from django.shortcuts import render_to_response
from blog.models import Tseclass,Tuser
from django.http import HttpResponseRedirect
from django.template.context import RenderContext
from django.views.decorators.csrf import csrf_exempt
from django.core.context_processors import csrf
# Create your views here.

def main(req):
    a = Tseclass.objects.filter(parent_secl_id=None)
    det={}
    for i in range(a.count()):
        seclassid = a.values()[i]['seclass_id']
        name = a.values()[i]['seclass_name']
        aa = Tseclass.objects.filter(parent_secl_id=seclassid)
        detdet={}
        for ii in range(aa.count()):
            seclassidid = aa.values()[ii]['seclass_id']
            name1 = aa.values()[ii]['seclass_name']
            d = Tseclass.objects.filter(parent_secl_id=seclassidid)
            detdet[name1]=d
        det[name]=detdet
    return render_to_response('main.html',{'det':det})

def test(req):
    a = Tseclass.objects.filter(parent_secl_id=None)
    det={}
    for i in range(a.count()):
        seclassid = a.values()[i]['seclass_id']
        name = a.values()[i]['seclass_name']
        aa = Tseclass.objects.filter(parent_secl_id=seclassid)
        detdet={}
        for ii in range(aa.count()):
            seclassidid = aa.values()[ii]['seclass_id']
            name1 = aa.values()[ii]['seclass_name']
            d = Tseclass.objects.filter(parent_secl_id=seclassidid)
            detdet[name1]=d
        det[name]=detdet
    return render_to_response('test.html',{'det':det})

def show(req):
    a=Tuser.objects.all()
    name=[]
    userinfo={}
    for i in range(a.count()):
        idT = a.values()[i]['idtuser']
        tuid = a.values()[i]['tu_id']
        uname = a.values()[i]['username']
        ulogin = a.values()[i]['login_name']
        seleve = a.values()[i]['seclevel']
        name=[idT,tuid,uname,ulogin,seleve]
        userinfo[tuid]=name
    return render_to_response('table.html',{'userinfo':userinfo})

def beginAdd(req):
    return render_to_response('add.html')

@csrf_exempt
def add(req):
   # c={}
    idtuser=req.POST['idtuser']
    tu_id =req.POST['tu_id']
    login_name=req.POST['login_name']
    password = req.POST['passwd']
    name = req.POST['name']
    seclevel = req.POST['level']
    st=Tuser()
    if  len(idtuser)  > 0 :
        print("id is not null")
        st.id=id
    st.idtuser=idtuser
    st.tu_id=tu_id
    st.login_name=login_name
    st.username=name
    st.seclevel = seclevel
    st.password=password
    st.save()
    return HttpResponseRedirect("/table")


def delByID(request):
    id=request.GET['id']
    bb=Tuser.objects.get(tu_id=id)
    bb.delete()
    return HttpResponseRedirect("/table")

def showUid(req):
    tuid = req.GET['id']
    bb = Tuser.objects.get(tu_id=tuid)
    return render_to_response('update.html',{'data':bb})

