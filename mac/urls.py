from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'mac.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
	url(r'^main/$','blog.views.main'),
    url(r'^test/$','blog.views.test'),
    url(r'^table/$','blog.views.show'),
    url(r'^index.html$','blog.views.beginAdd'),
    url(r'^add$','blog.views.add'),
    url(r'delete$','blog.views.delByID'),
    url(r'showid$','blog.views.showUid'),
    url(r'^admin/', include(admin.site.urls)),
)
